import React from 'react';
import logo from './avatar.png';
import './UserEssentials.css'


function UserEssentials() {
  return (
    <div className="UserEssentials">
      <img src={logo} alt="logo" />
      <div className="Client-info">
        <h3 className="Name">Nome Sobrenome</h3>
      </div>
    </div>
  );    
}

export default UserEssentials;
