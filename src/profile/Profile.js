import React from 'react';
import './Profile.css'
import UserInfo from './UserInfo.js';
import UserEssentials from './UserEssentials';


function Profile() {
  return (
    <div className="Profile">
      <UserEssentials />
      <UserInfo />
    </div>
  );    
}

export default Profile;
