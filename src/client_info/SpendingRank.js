import React from 'react';
import dollar from './dollar.svg'
import './SpendingRank.css'

function SpendingRank() {
  return (
    <div className="SpendingRank">
      <div className="Title">
        <h4>Risco<br/>Gastador</h4>
        <img src={dollar} alt="" />
      </div>
      <div className="Rank">
        <h1>1°</h1>
        <h2>/290</h2>
      </div>
      <p>informações do cliente</p>
    </div>
    
  );
}

export default SpendingRank;