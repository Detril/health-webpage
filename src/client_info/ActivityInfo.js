import React from 'react';
import './ActivityInfo.css'

function ActivityInfo() {
  return (
    <div className="ActivityInfo">
      <div className="Internations">
          <h3>Internação:</h3>
          <div className="Frequency">
            <h1>1</h1>
            <p>a cada 2 meses e<br/>21 dias</p>
          </div>
      </div>
      <div className="SubInfo">
        <div className="Exams">
          <h3>Exames:</h3>
          <p>Nenhum exame nos últimos <b>2 meses</b></p>
        </div>
        <div className="PS">
          <h3>PS:</h3>
          <p>Nenhuma consulta em <b>6 meses</b></p>
        </div>
      </div>
    </div>
    
  );
}

export default ActivityInfo;