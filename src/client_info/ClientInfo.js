import React from 'react';
import './ClientInfo.css'
import SpendingRank from './SpendingRank.js'
import ActivityInfo from './ActivityInfo.js'

function ClientInfo() {
  return (
    <div className="ClientInfo">
      <SpendingRank />
      <ActivityInfo />
    </div>
    
  );
}

export default ClientInfo;