import React from 'react';
import Header from './header/Header.js';
import Profile from './profile/Profile.js';
import ClientInfo from './client_info/ClientInfo.js';
import CostGraph from './CostGraph.js'
import './App.css';


function App() {
  return (
    <div className="App">
      <Header/>
      <Profile/>
      <div className="Infos">
        <ClientInfo />
        <div className="Observations">
          <h1>Observações Blue:</h1>
          <ul>
            <li>Possível quadro diabético</li>
            <li>Possível evolução para AVC</li>
            <li>Possível infecção urinária</li>
          </ul>
          <button>VER HISTÓRICO</button>
        </div>
      </div>
      <CostGraph />
    </div>
  );
}

export default App;
