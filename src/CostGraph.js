import React, {useState, useEffect, useRef, useCallback} from 'react';
import './CostGraph.css'

const cost_values = [150, 500, 1300, 1700, 2000, 900, 1100, 2300, 150, 500, 1300, 1700, 2000, 900, 1100, 2300]

function CostGraph() {
  const myRef = useRef(null);
  const [points, setPoints] = useState("")
  const height = 100
  const width = 680

  useEffect(() => {
    let cost_points = "";
    const p_w = width/(cost_values.length -1); // distance between points
    const p_h = (0.95 * height)/Math.max.apply(null, cost_values) // height of 1 cost value
    cost_values.forEach((val, i) => {
      const w = (i * p_w).toString();
      const h = (height - val * p_h).toString();
      cost_points += ' ' + w + ',' + h;
    });
    console.log(cost_points)
    setPoints(cost_points);
  }, []);

  return (
    <div className="CostGraph" >
      <div className="LabelAndGraph">
        <h4>Gastos</h4>
        <svg height={height} width={width - 60}>
          <polyline points={points} />
        </svg>
      </div>
      <h4>Tempo</h4>
    </div>
  );
}

export default CostGraph;