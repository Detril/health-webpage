import React from 'react';
import logo from './blue-logo.png';
import avatar from '../profile/avatar.png'
import './Header.css'
import SearchBar from './SearchBar.js'

function Header() {
  return (
    <div className="Header">
      <a href="/">
        <img src={logo} alt="logo" />
      </a>
      <SearchBar />
      <div className="Buttons">
        <button className="Feedback">Feedback</button>
        <button className="Company">Company</button>
      </div>
      <div className="Options">
        <img src={avatar} alt="user" />
      </div>
    </div>
    
  );
}

export default Header;