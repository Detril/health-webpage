import React from 'react';
import search_icon from './magnifying-glass.svg'
import './SearchBar.css'

function Header() {
  return (
    <div className="SearchBar">
      <img src={search_icon} alt="search" />
      <input type="text" className="Input" placeholder="Search..." />
    </div>
  );
}

export default Header;